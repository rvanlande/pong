Entity = Object:extend()

function Entity:new(x , y, width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.previousCheckIsCollide = false
end

function Entity:checkCollision(e)
    local a_left = self.x
    local a_right = self.x + self.width
    local a_top = self.y
    local a_bottom = self.y + self.height

    local b_left = e.x
    local b_right = e.x + e.width
    local b_top = e.y
    local b_bottom = e.y + e.height

    return b_left < a_right and b_right > a_left and b_top < a_bottom and b_bottom > a_top
end