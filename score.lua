Score = Object:extend()

function Score:new(x, y)
    self.x = x
    self.y = y
    self.value = 0
end

function Score:draw()
    love.graphics.print(self.value, self.x, self.y, 0, 1, 1)
end

function Score:inc()
    self.value = self.value + 1
    if self.value >= SCORE_TO_WIN then 
        game:setState(STATE_END)
    end        
end