STATE_NEW = 1
STATE_START_POINT_PLAYER1 = 2
STATE_START_POINT_PLAYER2 = 3
STATE_IN_GAME = 4
STATE_END = 5

Game = Object:extend()

function Game:new()
    self:setState(STATE_NEW)
end

function Game:draw()

    if self.state == STATE_NEW then

        love.graphics.setFont(bigFont)
        love.graphics.print("PONG", SCREEN_WIDTH / 2 - 150, SCREEN_HEIGHT / 2 - 200)
        love.graphics.setFont(smallFont)
        love.graphics.print("Press \"space\" to start game", SCREEN_WIDTH / 2 - 250, SCREEN_HEIGHT / 2)
        love.graphics.print("Press \"Q\" to quit game.", SCREEN_WIDTH / 2 - 250, SCREEN_HEIGHT / 2 + 40)

    elseif game:getState() ~= STATE_NEW then

        love.graphics.setFont(smallFont)
        -- love.graphics.print("FPS => " .. love.timer.getFPS(), 40, 40)
        topWall:draw()
        bottomWall:draw()
        middleWall:draw()
        player1:draw()
        player2:draw()
        ball:draw()
        love.graphics.setFont(bigFont)
        scorePlayer1:draw()
        scorePlayer2:draw()

        if game:getState() == STATE_END then
            love.graphics.setFont(bigFont)

            local _player = nil
            if scorePlayer1.value < scorePlayer2.value then
                _player = "2"
            else
                _player = "1"
            end
            love.graphics.print(" PLAYER " .. _player .. " WINS !", 100, SCREEN_HEIGHT / 2 - 200)
        end
    end

end

function Game:setState(state)
    self.state = state

    if self.state == STATE_NEW then
        scorePlayer1.value = 0
        scorePlayer2.value = 0
    elseif self.state == STATE_START_POINT_PLAYER1 then
        self:startPoint()
        ball.angle = 0
    elseif self.state == STATE_START_POINT_PLAYER2 then
        self:startPoint()
        ball.angle = math.pi
    end    
end

function Game:getState()
    return self.state
end

function Game:startPoint()
    player1:initPosition()
    player2:initPosition()
    ball.speed = START_SPEED
end