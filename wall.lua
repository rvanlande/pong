Wall = Entity:extend()

function Wall:draw()
    love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end

function Wall:isTopWall()
    return self.y == 0
end
