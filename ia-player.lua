IaPlayer = Player:extend()

function IaPlayer:new(x , y)
    IaPlayer.super.new(self, x , y)
    self.reachedNextPosition = true
    self.speed = 400
end

function IaPlayer:update(dt)

    if self.reachedNextPosition then
        if game:getState() == STATE_START_POINT_PLAYER2 then
            game:setState(STATE_IN_GAME)
            soundHit:play()
        end
        return
    end

    if ball.nextBallDestination ~= nil and self.y + self.height / 2 < ball.nextBallDestination.y then
        self.y = self.y + self.speed * dt
        self.reachedNextPosition = self.y >= ball.nextBallDestination.y
    elseif ball.nextBallDestination ~= nil and self.y + self.height / 2 > ball.nextBallDestination.y then
        self.y = self.y - self.speed * dt
        self.reachedNextPosition = self.y <= ball.nextBallDestination.y
    end 

    if self.y < WALL_WIDTH then
        self.y = WALL_WIDTH
    elseif self.y + PLAYER_HEIGHT > SCREEN_HEIGHT - WALL_WIDTH then
        self.y = SCREEN_HEIGHT - WALL_WIDTH - PLAYER_HEIGHT
    end
end

function IaPlayer:initPosition()
    IaPlayer.super.initPosition(self)
    self.reachedNextPosition = true
    if game:getState() == STATE_START_POINT_PLAYER2 then
        ball.nextBallDestination.y = math.random(SCREEN_HEIGHT - self.height - 20) + 20
        self.reachedNextPosition = false
    end
end