function love.load()

    love.window.setMode(1200, 800)

    -- constants
    SCREEN_WIDTH = love.graphics.getWidth()
    SCREEN_HEIGHT = love.graphics.getHeight()
    WALL_WIDTH = 20
    PLAYER_WIDTH = 20
    PLAYER_HEIGHT = 100
    BALL_WIDTH = 20
    SCORE_TO_WIN = 6
    math.randomseed(os.time())

    -- requires
    Object = require("lib/classic")
    require("game")
    require("entity")
    require("wall")
    require("player")
    require("ia-player")
    require("ball")
    require("score")

    -- objects
    topWall = Wall(0,0, SCREEN_WIDTH, WALL_WIDTH)
    bottomWall = Wall(0,SCREEN_HEIGHT - WALL_WIDTH, SCREEN_WIDTH, WALL_WIDTH)
    middleWall = Wall(SCREEN_WIDTH / 2 - (WALL_WIDTH / 2), 0,  WALL_WIDTH, SCREEN_HEIGHT)
    scorePlayer1 = Score(SCREEN_WIDTH / 2 - (WALL_WIDTH / 2) - 120, 40) 
    scorePlayer2 = Score(SCREEN_WIDTH / 2 + (WALL_WIDTH / 2) + 50, 40)
    ball = Ball(BALL_WIDTH, BALL_WIDTH)
    game = Game()
    player1 = Player("left")
    player2 = IaPlayer("right")
    game:setState(STATE_NEW)

    -- font
    smallFont = love.graphics.newFont("asset/arcade.ttf", 32)
    bigFont = love.graphics.newFont("asset/arcade.ttf", 128)

    -- sound
    soundBeep = love.audio.newSource("asset/beep.mp3", "static")
    soundHit = love.audio.newSource("asset/hit.mp3", "static")
end

function love.draw()
    game:draw()
end

function love.update(dt)

    if game:getState() ~= STATE_NEW and game:getState() ~= STATE_END then
        player1:update(dt)
        player2:update(dt)
        ball:update(dt)
    
        ball:resolveWallCollision(topWall)
        ball:resolveWallCollision(bottomWall)
        ball:resolvePlayerCollision(player1)
        ball:resolvePlayerCollision(player2)
    end
    
end

function love.keypressed(key)

    if key == 'q' then
        if game:getState() == STATE_NEW then
            love.event.quit()
        else
            game:setState(STATE_NEW)
        end
    end

    if game:getState() == STATE_NEW and key == 'space' then
        game:setState(STATE_START_POINT_PLAYER1)
    elseif game:getState() == STATE_START_POINT_PLAYER1 and key == 'space' then
        game:setState(STATE_IN_GAME)
        -- compute next intersect point ball<->player
        ball.nextBallDestination = ball:pointIntersection(player2)
        soundHit:play()
    end
end