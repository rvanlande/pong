START_SPEED = 600

Ball = Entity:extend()

function Ball:new(width, height)
    Ball.super.new(self, 0 , 0, width, height)        
    self.speed = START_SPEED
    self.nextBallDestination = {}
end

function Ball:draw()
    if game:getState() ~= STATE_END then
        love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
    end
end

function Ball:update(dt)

    if game:getState() == STATE_START_POINT_PLAYER1 then
        self.x = player1.x + player1.width
        self.y = (player1.y + player1.height / 2) - (self.height / 2)
    elseif game:getState() == STATE_START_POINT_PLAYER2 then
        self.x = player2.x - self.width
        self.y = (player2.y + player2.height / 2) - (self.height / 2)
    elseif game:getState() == STATE_IN_GAME then
        self.x = self.x + math.cos(self.angle) * dt * self.speed
        self.y = self.y - math.sin(self.angle) * dt * self.speed
    end

    if self.x < 0 then
        game:setState(STATE_START_POINT_PLAYER2)
        scorePlayer2:inc()
        soundBeep:play()
    elseif self.x > SCREEN_WIDTH then
        game:setState(STATE_START_POINT_PLAYER1)
        scorePlayer1:inc()
        soundBeep:play()
    end
end

function Ball:resolveWallCollision(wall)

    if (wall:isTopWall() and self.y < wall.y + wall.height) 
        or (not wall:isTopWall() and self.y + self.height > wall.y) then

        if not wall.previousCheckIsCollide then
            self.angle = -self.angle
            self:incSpeed()
            soundHit:play()
        end
        wall.previousCheckIsCollide = true

    else
        wall.previousCheckIsCollide = false
    end
end

function Ball:resolvePlayerCollision(player)
    
    if self:checkCollision(player) then
        if not player.previousCheckIsCollide then
            -- compute ball angle
            local _distancePosition = (player.y + player.height / 2) - (self.y + self.height / 2)
            local _maxdistancePosition = player.height / 2 + self.height / 2
            local _baseAngle = math.asin(_distancePosition / _maxdistancePosition)
            if _baseAngle > math.pi / 5 then 
                _baseAngle = math.pi / 5
            elseif _baseAngle < -math.pi / 5 then
                _baseAngle = -math.pi / 5
            end

            if player == player1 then
                self.angle = _baseAngle
                -- compute next intersect point ball<->player
                self.nextBallDestination = self:pointIntersection(player2)
            elseif player == player2 then
                self.angle = math.pi - _baseAngle
            end
            -- compute speed
            self:incSpeed()
            soundHit:play()
        end
        player.previousCheckIsCollide = true
    else
        player.previousCheckIsCollide = false
    end
end

function Ball:pointIntersection(player)

    -- reinit player reach final position
    player.reachedNextPosition = false

    -- compute absolute intersection point
    local _a = -math.tan(self.angle)
    local _b = self.y - _a * self.x
    local _x = player.x

    local _absoluteIntersection = {
        x = _x,
        y = _a * _x + _b,
    }

    -- compute screen intersection point
    local _courtHeight = SCREEN_HEIGHT - 2 * WALL_WIDTH
    local _modulo = math.floor(_absoluteIntersection.y) % _courtHeight
    local _isEven = math.floor((_absoluteIntersection.y / _courtHeight)) % 2 == 0
    local _ballDestinationY = nil
    if _isEven then
        _ballDestinationY = _modulo
    else
        _ballDestinationY = _courtHeight - _modulo
    end

    return {
        x = _absoluteIntersection.x,
        y = _ballDestinationY
    }
end

function Ball:incSpeed()
    self.speed = self.speed + 50
end

