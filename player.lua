Player = Entity:extend()

function Player:new(position)
    Player.super.new(self, 0 , 0, PLAYER_WIDTH, PLAYER_HEIGHT)  
    self.position = position
    self.speed = 1000
    self:initPosition()
end

function Player:initPosition()
    if self.position == "left" then
        self.x = 20
        self.y = (SCREEN_HEIGHT / 2) - (PLAYER_HEIGHT / 2)
    elseif self.position == "right" then
        self.x = SCREEN_WIDTH - 20 - PLAYER_WIDTH
        self.y = SCREEN_HEIGHT / 2 - (PLAYER_HEIGHT / 2)
    end
end

function Player:draw()
    love.graphics.rectangle('fill', self.x, self.y, PLAYER_WIDTH, PLAYER_HEIGHT)
end

function Player:update(dt)
    
    if love.keyboard.isDown('down') then
        self.y = self.y + self.speed * dt
    elseif love.keyboard.isDown('up') then
        self.y = self.y - self.speed * dt
    end

    if self.y < WALL_WIDTH then
        self.y = WALL_WIDTH
    elseif self.y + PLAYER_HEIGHT > SCREEN_HEIGHT - WALL_WIDTH then
        self.y = SCREEN_HEIGHT - WALL_WIDTH - PLAYER_HEIGHT
    end
end